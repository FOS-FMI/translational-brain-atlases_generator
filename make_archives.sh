#!/usr/bin/env bash

while getopts ':v:n:m' flag; do
	case "${flag}" in
		v)
			PV="$OPTARG"
			;;
		n)
			PN="$OPTARG"
			;;
		:)
			echo "Option -$OPTARG requires an argument." >&2
			exit 1
			;;
	esac
done

if [ -z "$PV" ]; then
	PV=9999
fi

if [ -z "$PN" ]; then
	PN="translational-brain-atlases"
fi

P="${PN}-${PV}"

mkdir ${P}
cp FAIRUSE-AND-CITATION ${P}
pushd ${P}
	bash ../abi.sh || exit 1
	bash ../abi2rat.sh || exit 1
	# it might be best to remove trace .nii files here, see the mouse-brain-templates_generator package for details.
popd

tar cfJ "${P}.tar.xz" ${P}
str=$(sha512sum ${P}.tar.xz); echo "${str%% *}" > "${P}.sha512"
