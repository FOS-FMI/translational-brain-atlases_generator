import nibabel as nib
import numpy as np


image = nib.load('abi2w_MARPN_15micron_labels.nii')
#data = img.get_fdata()
#data=data.astype(np.int16)

#new_img = nib.Nifti1Image(data, img.affine, img.header)
#nib.save(new_img, "abi2dsurqec_15micron_labels_altered.nii")

# to be extra sure of not overwriting data:
new_data = np.copy(image.get_data())
hd = image.header

# in case you want to remove nan:
#new_data = np.nan_to_num(new_data)

# update data type:
new_dtype = np.int16  # for example to cast to int8.
new_data = new_data.astype(new_dtype)
image.set_data_dtype(new_dtype)

# if nifty1
if hd['sizeof_hdr'] == 348:
    new_image = nib.Nifti1Image(new_data, image.affine, header=hd)
# if nifty2
elif hd['sizeof_hdr'] == 540:
    new_image = nib.Nifti2Image(new_data, image.affine, header=hd)
else:
    raise IOError('Input image header problem')

nib.save(new_image, 'abi2w_MARPN_15micron_labels.nii')
