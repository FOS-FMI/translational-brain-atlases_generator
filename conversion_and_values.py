import os
import nrrd
import nibabel
import numpy as np
import sys
import math
import fileinput


def replace_with_dict2(ar, dic):
    # Extract out keys and values
    k = np.array(list(dic.keys()))
    v = np.array(list(dic.values()))

    # Get argsort indices
    sidx = k.argsort()

    ks = k[sidx]
    vs = v[sidx]
    return vs[np.searchsorted(ks,ar)]


path = os.path.abspath('.')
nrrd_file = os.path.abspath(os.path.expanduser("abi_10micron_labels.nrrd"))

print("Reading " + nrrd_file)
readnrrd = nrrd.read(nrrd_file)
data = readnrrd[0]
header = readnrrd[1]


print("Creating dictionary")
#len_matrix=len(data[0])*len(data[0][0])
#len_line=len(data[0][0])
data_flat=data.flatten()
values = set(data_flat)
values = sorted(list(values))


values_length = len(values)
new_values = list(range(values_length))

values_dict = dict(zip(values,new_values))

#edit annotations based on dict

print("Editing annotation (json) file")

annotation_file="abi_annotation.json"

lines=[]

new_ids=0

with open(annotation_file) as annotation:
    for line in annotation:
        if(line.find("\"id\": ")!=-1):
           # print(line)
           # x = line.split("\": ")
            start = line.find("\": ") + len("\": ")
            end = line.find(",")
            original = line[start:end]
            if int(original) in values_dict.keys():
                    new = str(values_dict[int(original)])

            else:
                    new =str(values_length+new_ids)
                    new_ids=new_ids+1

            line=line.replace(original, new)
           # print(new)
           # print(line)
        lines.append(line)

with open(annotation_file, "w+") as f:
    for i in lines:
        f.write(i)


"""
f = open(annotation_file,'r')
filedata = f.read()
f.close()

substr="\"id\": "

res = [i for i in range(len(filedata)) if filedata.startswith(substr, i)]

for i in res:

f = open(annotation_file,'w')
f.write(newdata)
f.close()
"""


print("Altering nrrd data")
#edit data based on dict

new_data = replace_with_dict2(data,values_dict)
#new_data_values = sorted(list(set(new_data.flatten())))




"""
print(data.shape)
print(new_data.shape)

print(values[-10:])
print(new_values[-10:])
print(new_data_values[-10:])

print(len(new_data_values))
print(len(new_values))
"""

data=new_data.astype(np.int16)

# value conversion goes here, new_data will be the product, a 3D array

affine_matrix = np.array(header["space directions"],dtype=np.float)
#if space[0] == 'left':
#    affine_matrix[0,0] = affine_matrix[0,0] * (-1)
#if space[1] == 'posterior':
#    affine_matrix[1,1] = affine_matrix[1,1] * (-1)
#Units?
affine_matrix = affine_matrix*0.001

affine_matrix = np.insert(affine_matrix,3,[0,0,0], axis=1)
affine_matrix = np.insert(affine_matrix,3,[0,0,0,1], axis=0)

#Change Orientation from PIR to RAS. Steps: PIR -> RIP -> RPI -> RPS -> RAS
data.setflags(write=1)
data = np.swapaxes(data,0,2)
data = np.swapaxes(data,1,2)
data = data[:,:,::-1]
data = data[:,::-1,:]
#
img = nibabel.Nifti1Image(data,affine_matrix)
nibabel.save(img,os.path.join(path, os.path.basename(nrrd_file).split(".")[0] + '.nii'))
#
#Delete Nrrd-File
os.remove(nrrd_file)

