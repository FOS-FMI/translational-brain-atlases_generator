#!/usr/bin/env bash

# donwload average template from ABI Website (we don't distribute this, but rather need it for the registration)
wget -O abi_10micron_average.nrrd http://download.alleninstitute.org/informatics-archive/current-release/mouse_ccf/average_template/average_template_10.nrrd

# download labels (we disambiguate the upstream nomenclature, labels meaning the numeric identifiers, and annotation the human-readable categorization)
wget -O abi_10micron_labels.nrrd http://download.alleninstitute.org/informatics-archive/current-release/mouse_ccf/annotation/ccf_2017/annotation_10.nrrd

# download annotation (we disambiguate the upstream nomenclature, labels meaning the numeric identifiers, and annotation the human-readable categorization)
wget -O abi_annotation.json http://api.brain-map.org/api/v2/structure_graph_download/1.json

# Convert to Nifti and reorient data matrix from PIR to RAS
python nrrd_to_nifti.py

# Adjust the label values space to be continuous
python conversion_and_values.py

