import os
from glob import glob
import nrrd
import nibabel
import numpy as np
import sys
import math

#path = os.path.dirname(sys.argv[0])
path = os.path.abspath('.')

myfile = os.path.abspath(os.path.expanduser("abi_10micron_average.nrrd"))




print("Reading " + myfile)
readnrrd = nrrd.read(myfile)
data = readnrrd[0]
header = readnrrd[1]

print("Converting " + myfile)

#if space[0] == 'left':
#    affine_matrix[0,0] = affine_matrix[0,0] * (-1)
#if space[1] == 'posterior':
#affine_matrix[1,1] = affine_matrix[1,1] * (-1)
#Units?
affine_matrix = np.array(header["space directions"],dtype=np.float)

affine_matrix = affine_matrix*0.001

affine_matrix = np.insert(affine_matrix,3,[0,0,0], axis=1)
affine_matrix = np.insert(affine_matrix,3,[0,0,0,1], axis=0)

#Change Orientation from PIR to RAS. Steps: PIR -> RIP -> RPI -> RPS -> RAS
data.setflags(write=1)
data = np.swapaxes(data,0,2)
data = np.swapaxes(data,1,2)
data = data[:,:,::-1]
data = data[:,::-1,:]
#
img = nibabel.Nifti1Image(data,affine_matrix)
nibabel.save(img,os.path.join(path, os.path.basename(myfile).split(".")[0] + '.nii'))
#
#Delete Nrrd-File
os.remove(myfile)



