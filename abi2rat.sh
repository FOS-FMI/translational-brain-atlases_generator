#!/bin/bash

#edited from abi2dsurqec.sh


#STANDALONE=$1
#
##Standalone: Need to download atlas and transform beforehand
#if [ -n "${STANDALONE}" ]; then
#	bash dsurqec.sh
#	bash ambmc.sh
#	rm lambmc*
#	rm ldsurqec_*
#	rm dsurqec_200*
#	rm ambmc_200*
#fi

# Resize
## obtain a version of the w_MARPN template at 15µm

cp /home/tara/src/rat-brain-templates_generator/rat-brain-templates-0.1/w_MARPN_pnd_40_25micron_masked.nii.gz .
gunzip w_MARPN_pnd_40_25micron_masked.nii.gz

ResampleImage 3 w_MARPN_pnd_40_25micron_masked.nii _w_MARPN_15micron_masked.nii 0.015x0.015x0.015 0 0 4
SmoothImage 3 _w_MARPN_15micron_masked.nii 0.4 w_MARPN_15micron_masked.nii
fslorient -copyqform2sform w_MARPN_15micron_masked.nii
rm _w_MARPN_15micron_masked.nii


## obtain a version of the abi atlas at 15 µm
ResampleImage 3 abi_10micron_average.nii abi_15micron_average.nii 0.015x0.015x0.015 0 0 4
fslorient -copyqform2sform abi_15micron_average.nii

antsApplyTransforms -d 3 -e 0 -i abi_10micron_labels.nii -r abi_15micron_average.nii -o abi_15micron_labels.nii -n MultiLabel -t Identity
fslorient -copyqform2sform abi_15micron_labels.nii
#antsApplyTransforms -d 3 -e 0 -i abi_10micron_labels.nii -r abi_40micron_average.nii -o abi_40micron_labels.nii -n MultiLabel -t Identity
#fslorient -copyqform2sform abi_40micron_labels.nii

##### we might not actually need this, but keep it in for the first run and we can see if we can delete it later
#Run AntsRegisatr
antsAI -d 3 -v \
		--transform Rigid[ 0.5 ] \
		--metric MI[w_MARPN_15micron_masked.nii,ambmc_15micron.nii, 1, 64, Random, 0.1 ] \
		exit 1

# Registration call

#SyN registration restricted to 3 Levels and never at full resolution (shrink factor 1, smooothing sigmas 0). This will not affect size of output file,
#but the final displacement field (.h5 file) will not be the correct size. If needed, add a token iteration at full resolution. This will increase memory consumption considerably.
antsRegistration \
	--float 1 \
	--collapse-output-transforms 1 \
	--dimensionality 3 \
	--initial-moving-transform [w_MARPN_15micron_masked.nii,abi_15micron_average.nii, 1 ] \
	--initialize-transforms-per-stage 0 --interpolation Linear --output [ abi2w_MARPN_, abi2w_MARPN_15micron_masked.nii ] \
	--interpolation Linear \
	\
	--transform Rigid[ 0.5 ] \
	--metric MI[w_MARPN_15micron_masked.nii,abi_15micron_average.nii, 1, 64, Random, 0.1 ] \
	--convergence [ 400x400x400x200, 1e-9, 10 ] \
	--smoothing-sigmas 3.0x2.0x1.0x0.0vox \
	--shrink-factors 10x4x2x1 \
	--use-estimate-learning-rate-once 0 \
	--use-histogram-matching 1 \
	\
	--transform Affine[ 0.1 ]\
	--metric MI[w_MARPN_15micron_masked.nii,abi_15micron_average.nii, 1, 64, Regular, 0.1 ] \
	--convergence [ 400x200, 1e-10, 10 ] \
	--smoothing-sigmas 1.0x0.0vox \
	--shrink-factors 2x1 \
	--use-estimate-learning-rate-once 0 \
	--use-histogram-matching 1 \
	\
	--transform SyN[0.25,3,0] \
	--metric CC[w_MARPN_15micron_masked.nii,abi_15micron_average.nii,1,4] \
	--convergence [120x90x50,1e-6,10] \
	--shrink-factors 8x4x2 \
	--smoothing-sigmas 3x2x1vox \
	\
	--winsorize-image-intensities [ 0.05, 0.95 ] \
	--write-composite-transform 1 \
	--verbose
#changed transform SyN[0.25,3,0] to [0.25,2,0] to [0.25,1,0] to [0.25,2,0] to [0.27,2,0]
#abi2rat: changed transform SyN to [0.25,3,0]. not run yet

#changed SyN convergence from [100x70x50,1e-6,10] to [100x70x60,1e-6,10] to [100x80x60,1e-6,10] to [110x85x50,1e-6,10] to [120x90x50,1e-6,10]
fslorient -copyqform2sform abi2w_MARPN_15micron_masked.nii

#Use the composite to transform labels file
antsApplyTransforms -d 3 -i abi_15micron_labels.nii -r w_MARPN_15micron_masked.nii -o abi2w_MARPN_15micron_labels.nii -t "abi2w_MARPN_Composite.h5" -n MultiLabel
fslorient -copyqform2sform abi2w_MARPN_15micron_labels.nii

# Create labels files for all output resolutions.
ResampleImage 3 abi2w_MARPN_15micron_labels.nii abi2w_MARPN_40micron_labels.nii 0.04x0.04x0.04 0 0 4
fslorient -copyqform2sform abi2w_MARPN_40micron_labels.nii
ResampleImage 3 abi2w_MARPN_15micron_labels.nii abi2w_MARPN_200micron_labels.nii 0.2x0.2x0.2 0 0 4
fslorient -copyqform2sform abi2w_MARPN_200micron_labels.nii

ResampleImage 3 w_MARPN_15micron_masked.nii w_MARPN_40micron_masked.nii 0.04x0.04x0.04 0 0 4
fslorient -copyqform2sform w_MARPN_40micron_masked.nii

ResampleImage 3 abi2w_MARPN_15micron_masked.nii abi2w_MARPN_40micron_masked.nii 0.04x0.04x0.04 0 0 4
fslorient -copyqform2sform abi2w_MARPN_40micron_masked.nii

#reduce the file size by changing the datatype of integers in the nifti file to uint16.
python adjust_abi2w_MARPN_15labels.py 
