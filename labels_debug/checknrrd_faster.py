import nrrd
import os
from glob import glob
import numpy as np
import sys

nrrd_file = os.path.abspath(os.path.expanduser('abi_10micron_annotation.nrrd'))

print("Reading " + nrrd_file)
readnrrd = nrrd.read(nrrd_file)
print("Printing")
data = readnrrd[0]
header = readnrrd[1]
print(header)

data_flat = data.flatten()
unique_values = set(data_flat)
print(np.max(data_flat))
