import nrrd
import os
from glob import glob
import numpy as np
import sys

nrrd_file = os.path.abspath(os.path.expanduser('abi_10micron_annotation.nrrd'))

print("Reading " + nrrd_file)
readnrrd = nrrd.read(nrrd_file)
print("Printing")
data = readnrrd[0]
header = readnrrd[1]
print(header)

max = 0
for matrix in data:
    for row in matrix:
        for number in row:
            if(number>max):
                max=number
    print(".", end="")
print(max)
