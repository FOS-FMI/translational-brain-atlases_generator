import nrrd
import os
from glob import glob
import numpy as np
import sys
import matplotlib.pyplot as plt
#import pylab

nrrd_file = os.path.abspath(os.path.expanduser('abi_10micron_average.nrrd'))

print("Reading " + nrrd_file)
readnrrd = nrrd.read(nrrd_file)
print("Printing")
data = readnrrd[0]
header = readnrrd[1]
print(header)

data_flat = data.flatten()

fig, ax = plt.subplots()

ticks=[]

for i in range(10):
    ticks.append(10**i)



plt.hist(data_flat, bins=np.logspace(start=np.log10(1), stop=np.log10(10**9), num=10))
plt.gca().set_xscale("log")


plt.xticks(ticks)

plt.show()

# https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.hist.html

plt.savefig('checknrrd_histogram10template.png')

