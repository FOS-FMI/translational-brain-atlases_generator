
import nrrd
import os
from glob import glob
import numpy as np
import sys
import matplotlib.pyplot as plt
import math
#import pylab

nrrd_file = os.path.abspath(os.path.expanduser('abi_10micron_annotation.nrrd'))

print("Reading " + nrrd_file)
readnrrd = nrrd.read(nrrd_file)
print("Printing")
data = readnrrd[0]
header = readnrrd[1]
print(header)

data_flat = data.flatten()


fig, ax = plt.subplots()

ticks=[]
b=20
#n=np.log(6.5*10**8)/b
n=(614454277)/b

iterate=math.ceil(4*10**8/n)


for i in range(iterate):
    ticks.append((614454277-4*10**8)+n*i)



#plt.hist(data_flat, bins=np.logspace(start=np.log10(1), stop=np.log10(10**9), num=10))
#plt.gca().set_xscale("log")


plt.hist(data_flat, bins=b)
plt.xticks(ticks)

ax.set_xbound(3*10**8, 6.5*10**8)
ax.set_ybound(0, 10**7)


plt.show()

# https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.hist.html
#hist = plt.hist(data_flat, bins=20)
#plt.set_xscale('log')
#pylab.show()

plt.savefig('checknrrd_histogram_highvalues.png')

