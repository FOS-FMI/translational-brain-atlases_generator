# Rat brain template best suited for mouse-to-rat atlas transformation

`w_MARPN_pnd_40_25micron_masked.nii`

# Native rat brain atlases

1) The Rat Brain in Stereotaxic Coordinates, Paxinos and Watson (2007): Atlas in book form consisting of 161 coronal diagrams of a rat brain at 120 µm intervals, 19 sagittal diagrams, and 27 horizontal diagrams. Identifies over 1000 structures, and is widely considered a reputable rat brain atlas.

2) Ratat1: A Digital Rat Brain Stereotaxic Atlas Derived from High-Resolution MRI Images Scanned in Three Dimensions, Wisner et al (2016), https://www.frontiersin.org/articles/10.3389/fnsys.2016.00064/full  :  Atlas consists of a series of MR images in transverse, sagittal, and horizontal views of adult Long Evans rats with a resolution of 50 µm. Presented in pdf format.

3) An MRI-Derived Neuroanatomical Atlas of the Fischer 344 Rat Brain, Goerzen et al (2020), https://www.nature.com/articles/s41598-020-63965-x  :  3D MRI atlas developed through averaging the brain images of 41 adult Fischer 344 rats using Pydpiper image registration. Images were manually segmented and  71 structures and substructures were delineated. The atlas can be found in minc and nifti format. Paper also provided a table comparing this rat brain atlas to 6 other atlases (https://www.nature.com/articles/s41598-020-63965-x/tables/1), which are listed below.

4) Waxholm Space atlas of the Sprague Dawley rat brain, Papp et al (2014), https://www.sciencedirect.com/science/article/pii/S1053811914002419?via%3Dihub  :  3D atlas that uses both 39 μm MRI and 78 μm DTI templates, on which 76 anatomical structures are delineated (although these structures are segmented further). Atlas is of the Sprague Dawley rat brain and uses a spatial reference system based on the Waxholm Space Standard, a set of landmarks in the brain initially used for the mouse brain. Atlas is available in Nifti format. ANTs registration used for post processing.

5) An ontology-based segmentation scheme for tracking postnatal changes in the developing rodent brain with MRI, Calebrese et al (2013), https://www.sciencedirect.com/science/article/abs/pii/S1053811912011494?via%3Dihub  :  Atlas designed to track post-natal development of rat brains, and consists of an ontology (describes hierarchy of structures) and labels nifti files.

6) A stereotaxic MRI template set for the rat brain with tissue class distribution maps and co-registered anatomical atlas: application to pharmacological MRI, Schwarz et al. (2006), https://pubmed.ncbi.nlm.nih.gov/16784876/  :  phMRI templates (with apomorphine) co-registered with Paxinos and Watson atlas.

7) 3-dimensional diffusion tensor imaging (DTI) atlas of the rat brain, Rumple et al (2013), https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3702494/   :  Atlas is manually segmented and based on DTI templates of Sprague-Dawley rats at postnatal day 5, 14, and 72. Data presented in NRRD format.

8) A Multidimensional Magnetic Resonance Histology Atlas of the Wistar Rat Brain, Johnson et al (2012), https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3408821/  :  MR atlas created through comparison of Watson Paxinos atlas and histochemical and 

9) An in vivo MRI template set for morphometry, tissue segmentation, and fMRI localization in rats, Valdés-Hernández et al (2011), https://www.frontiersin.org/articles/10.3389/fninf.2011.00026/full
