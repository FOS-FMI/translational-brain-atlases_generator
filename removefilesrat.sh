# Remove all files from abi-to-rat atlas transformation
rm abi_10micron_average.nii           
rm abi_10micron_labels.nii            
rm abi_15micron_average.nii           
rm abi_15micron_labels.nii 
rm abi2w_MARPN_15micron_masked.nii
rm abi2w_MARPN_Composite.h5          
rm abi2w_MARPN_InverseComposite.h5
rm w_MARPN_15micron_masked.nii
rm abi2w_MARPN_200micron_labels.nii
rm abi2w_MARPN_40micron_labels.nii
rm abi2w_MARPN_15micron_labels.nii
rm w_MARPN_pnd_40_15micron_masked.nii 
rm w_MARPN_pnd_40_25micron_masked.nii 
